<?php
/**
 * Created by PhpStorm.
 * User: Agustin Llamas Ruiz
 */

namespace app\models;

class Product
{
    /**
     * @param $productId
     * @param $quantityAvailable
     * @param bool $cache
     * @param int $cacheDuration
     * @param null $securityStockConfig
     * @return false|int|mixed|null|string
     * @throws \Throwable
     */
    public static function stock($productId, $quantityAvailable, $cache = false, $cacheDuration = 60, $securityStockConfig = null)
    {
        $stock = $quantityAvailable;
        if($quantityAvailable >= 0){
            $stockBlocked = self::getStockBlocked($productId, $cache, $cacheDuration);
            $stock = $quantityAvailable - $stockBlocked;
            if($securityStockConfig){
                $stock = $stock - self::getQuantityStockInSecurity($stock, $securityStockConfig);
            }
        }

        return $stock > 0 ? $stock : 0;
    }

    /**
     * @param $quantity
     * @param $securityStockConfig
     * @return mixed
     */
    private static function getQuantityStockInSecurity($quantity, $securityStockConfig){
        return ShopChannel::applySecurityStockConfig(
            $quantity,
            @$securityStockConfig->mode,
            @$securityStockConfig->quantity
        );
    }

    /**
     * @param $productId
     * @param $cache
     * @param $cacheDuration
     * @return false|int|null|string
     * @throws \Throwable
     */
    private static function getStockBlocked($productId, $cache, $cacheDuration){
        if($cache){
            $quantity = intval(OrderLine::getQuantityOfProductInOrderInProcessAndCache($productId, $cacheDuration))
                + intval(BlockedStock::getQuantityProductsInShoppingCartAndCache($productId, $cacheDuration));
        }else{
            $quantity = intval(OrderLine::getQuantityOfProductInOrderInProcess($productId))
                + intval(BlockedStock::getQuantityProductsInShoppingCart($productId));
        }

        return $quantity;
    }
}