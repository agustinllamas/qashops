<?php
/**
 * Created by PhpStorm.
 * User: Agustin Llamas Ruiz
 */

namespace app\models;


use yii\db\ActiveRecord;

class OrderLine extends ActiveRecord
{
    /**
     * @param $productId
     * @return string
     */
    public static function getQuantityOfProductInOrderInProcess($productId)
    {
        return self::find()
            ->select('SUM(quantity) as quantity')
            ->joinWith('order')
            ->where("order.status IN ('" . implode("','", Order::getStatusInProcess()) . "') AND order_line.product_id = " . $productId)
            ->scalar();
    }

    /**
     * @param string $productId
     * @param int $cacheDuration
     * @return string
     * @throws \Throwable
     */
    public static function getQuantityOfProductInOrderInProcessAndCache($productId, $cacheDuration)
    {
        return self::getDb()->cache(self::getQuantityOfProductInOrderInProcess($productId), $cacheDuration);
    }
}