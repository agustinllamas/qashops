<?php
/**
 * Created by PhpStorm.
 * User: Agustin Llamas Ruiz
 * Date: 20/07/2019
 * Time: 13:36
 */

namespace app\models;


use yii\db\ActiveRecord;

class BlockedStock extends ActiveRecord
{
    /**
     * @param $productId
     * @return false|null|string
     */
    public static function getQuantityProductsInShoppingCart($productId){
        return self::find()
            ->select('SUM(quantity) as quantity')
            ->joinWith('shoppingCart')
            ->where("blocked_stock.product_id = ".$productId." AND blocked_stock_date > '" . date('Y-m-d H:i:s') . "' AND (shopping_cart_id IS NULL OR shopping_cart.status = '" . ShoppingCart::STATUS_PENDING . "')")
            ->scalar();
    }

    /**
     * @param $productId
     * @param $cacheDuration
     * @return mixed
     * @throws \Throwable
     */
    public static function getQuantityProductsInShoppingCartAndCache($productId, $cacheDuration){
        return self::getDb()->cache(self::getQuantityProductsInShoppingCart($productId), $cacheDuration);
    }
}