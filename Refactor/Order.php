<?php
/**
 * Created by PhpStorm.
 * User: Agustin Llamas Ruiz
 */

namespace app\models;


use yii\db\ActiveRecord;

class Order extends ActiveRecord
{
    CONST STATUS_PENDING = 1;
    CONST STATUS_PROCESSING = 2;
    CONST STATUS_WAITING_ACCEPTANCE = 3;

    public static function getStatusInProcess(){
        return [self::STATUS_PENDING, self::STATUS_PROCESSING, self::STATUS_WAITING_ACCEPTANCE];
    }

}