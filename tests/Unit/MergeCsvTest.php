<?php

namespace Tests\Unit;

use App\Http\src\MergeCsv;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MergeCsvTest extends TestCase
{
    /** @test */
    public function merge_two_csv()
    {
        $mergeCsv = new MergeCsv();

        $csv = $mergeCsv->merge(getcwd() . "/resources/test/csvA.csv", getcwd() . "/resources/test/csvB.csv");

        $this->assertSame(file_get_contents(getcwd() . "/resources/test/csvResult.csv"), file_get_contents($csv));

        unlink($csv);
    }
}
