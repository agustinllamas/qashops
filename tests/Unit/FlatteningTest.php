<?php

namespace Tests\Unit;

use App\Http\src\FlatteningProducts;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FlatteningTest extends TestCase
{
    /** @test */
    public function flattening_xml_file_to_csv()
    {
        $flat = new FlatteningProducts();

        $csv = $flat->flattering(getcwd() . "/resources/test/aplanamiento.xml");

        $this->assertSame(file_get_contents(getcwd() . "/resources/test/products.csv"), file_get_contents($csv));

        unlink($csv);
    }
}
