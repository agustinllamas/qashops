<?php
/**
 * Created by PhpStorm.
 * User: Agustin Llamas Ruiz
 */

namespace App\Http\src;

class FlatteningProducts
{
    /**
     * @param $xmlPath
     * @return string path file resultante
     */
    public function flattering($xmlPath)
    {
        $xml = file_get_contents($xmlPath);
        $products = $this->xmlToArray($xml);
        return $this->arrayToCsv($products['products']['product']);
    }

    private function arrayToCsv($products)
    {
        $fileName = $this->getFileName();
        $csvFile = fopen($fileName, 'w');
        $headers = $this->getHeaders($products);

        fputcsv($csvFile, $headers, ';');

        foreach($products as $product){
            $csvLine = [];
            foreach($headers as $header){
                $csvLine[] = isset($product[$header]) ? $product[$header] : '';
            }
            fputcsv($csvFile, $csvLine, ';');
        }

        fclose($csvFile);

        return $fileName;
    }

    private function getHeaders($products)
    {
        $headers = [];
        foreach($products as $p){
            $headers = array_merge($headers, array_keys($p));
        }

        return array_unique($headers);
    }

    /**
     * @param string $xmlFile
     * @return array
     */
    private function xmlToArray($xmlFile)
    {
        $xml = simplexml_load_string($xmlFile);
        $json  = json_encode($xml);
        return json_decode($json, true);
    }

    private function getFileName(){
        if(strpos(getcwd(), 'public') !== false){
            $fileName = getcwd() . '/../resources/csv/file-' . rand(1, 1000) . '.csv';
        }else{
            $fileName = getcwd() . '/resources/csv/file-' . rand(1, 1000);
        }

        return $fileName;
    }
}