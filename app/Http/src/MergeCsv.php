<?php
/**
 * Created by PhpStorm.
 * User: Agustin Llamas Ruiz
 * Date: 21/07/2019
 * Time: 1:14
 */

namespace App\Http\src;


class MergeCsv
{
    /**
     * @param $pathCsvA
     * @param $pathCsvB
     * @return string FileName Resultado
     */
    public function merge($pathCsvA, $pathCsvB)
    {
        $csvA = fopen($pathCsvA, 'r');
        $csvB = fopen($pathCsvB, 'r');

        $linesA = $this->csvToArray($csvA);
        $linesB = $this->csvToArray($csvB);
        $headers = $this->mergeHeaders($csvA, $csvB);

        return $this->constructCsvResult($headers, $linesA, $linesB);
    }

    private function constructCsvResult($headers, $linesA, $linesB)
    {
        $fileName = $this->getFileName();
        $csvFile = fopen($fileName, 'w');

        fputcsv($csvFile, $headers, ',');

        $this->putArrayIntoCsv($csvFile, $headers, $linesA);
        $this->putArrayIntoCsv($csvFile, $headers, $linesB);

        fclose($csvFile);

        return $fileName;
    }

    private function putArrayIntoCsv($fp, $headers, $lines)
    {
        foreach($lines as $line){
            $lineCsv = [];
            foreach ($headers as $header){
                $lineCsv[] = isset($line[$header]) ? $line[$header] : '';
            }
            fputcsv($fp, $lineCsv, ",");
        }
    }

    private function csvToArray($csv)
    {
        $lines = [];
        $headers = fgetcsv($csv, 0, ',');

        while($lineCsv = fgetcsv($csv, 0, ',')){
            $line = [];
            foreach($lineCsv as $key => $value){
                $line[$headers[$key]] = $value;
            }
            $lines[] = $line;
        }

        return $lines;
    }

    private function mergeHeaders($csvA, $csvB)
    {
        rewind($csvA);
        rewind($csvB);
        $headersA = fgetcsv($csvA, 0, ',');
        $headersB = fgetcsv($csvB, 0, ',');

        return array_unique(array_merge($headersA, $headersB));
    }

    private function getFileName(){
        if(strpos(getcwd(), 'public') !== false){
            $fileName = getcwd() . '/../resources/csv/file-' . rand(1, 1000) . '.csv';
        }else{
            $fileName = getcwd() . '/resources/csv/file-' . rand(1, 1000) . '.csv';
        }

        return $fileName;
    }
}