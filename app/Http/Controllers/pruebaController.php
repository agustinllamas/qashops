<?php

namespace App\Http\Controllers;

use App\Http\src\FlatteningProducts;
use App\Http\src\MergeCsv;
use Illuminate\Http\Request;

class pruebaController extends Controller
{
    public function flattering()
    {
        $flat = new FlatteningProducts();
        $csv = $flat->flattering(getcwd() . "/../resources/test/aplanamiento.xml");

        return file_get_contents($csv);
    }

    public function mergeCsv()
    {
        $mergeCsv = new MergeCsv();
        $csv = $mergeCsv->merge(getcwd() . "/../resources/test/csvA.csv", getcwd() . "/../resources/test/csvB.csv");

        return file_get_contents($csv);
    }
}
