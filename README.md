## QaShops prueba técnica PHP

Este proyecto se ha creado para una prueba técnica para la empresa QaShops.

Esta realizado en Larabel

Consta de dos ejercicios :

- Refactorización de una clase
- Aplanamiento de un archivo XML en un archivo CSV
- Fusión de dos ficheros CSV en un fichero CSV

## Refactorización

Se ha agregado un directorio en la raíz del proyecto llamado "**Refactor**" con los ficheros del primero ejercicio
que había que realizar una refactorización de la clase Producto. Esta realizado con código del framework Yii2 por 
consiguiente se pone aparte.

## Aplanamiento de XML

Se ha creado una clase llamada "**flatteningProducts**" donde dado un xml que contiene productos en formato XML se realiza
el aplanamiento para convertirlos en líneas de un csv separados por ";"

Dicha clase contiene un solo método publico llamado "**flattering**" que recibe el path de un fichero xml y devuelve
el path de un fichero csv con el resultado

Para ello se convierte el fichero xml en un array asociativo para después poder trasladar dichos arrays al CSV.

Path de la clase: app/Http/src/FlatteningProducts.php

## Fusión de CSVs

Se ha creado una clase llamada "**MergeCsv**" donde dado dos ficheros CSV los fusiona para convertirlo en un solo CSV
quitando columnas duplicadas

Dicha clase contiene un solo método llamado "**merge**" que recibe el path de dos ficheros CSV y devuelve un solo CSV 
con la fusión

Para ello se crean dos arrays asociativos para cada fichero CSV, para luego unir las cabeceras de los ficheros e
incorporar las líneas en sus respectivas columnas.

Path de la clase: app/Http/src/MergeCsv.php

##Pruebas unitarias

Se han creado pruebas unitarias para comprobar si funcionan las clases de aplanamiento y fusión. 

Directorio "test/Unit"

FlatteningText Para el aplanamiento

MergeCsvText PAra la fusión

Para ello se ha incorporado ficheros xml y csvs de los ejemplos y se comprueba que el resultado es correcto.